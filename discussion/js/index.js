// Create pokemon object
let pokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function () {
		console.log("This pokemon tackled targetPokemon")
	},

	faint: function() {
		console.log("Pokemon fainted")
	}
}


// Create pokemon object constructor
function Pokemon(name, level) {
	this.name = name
	this.level = level
	this.health = level * 3
	this.attack = level,

	this.faint = function() {
		console.log(this.name + " fainted.")
	},
	this.tackle = function (target) {
		console.log(target.health)
		console.log(this.attack)
		console.log(this.name + " tackled " + target.name)
		target.health = target.health - this.attack 
		
		target.health <= 5 ?  target.faint():console.log(target.name + "'s health is now reduced to " + target.health)
		
	}

	
}

// Instantiate object using its constructor
let pikachu = new Pokemon("Pikachu", 16)
let squirtle = new Pokemon("Squirtle", 8)

pikachu.tackle(squirtle)
pikachu.tackle(squirtle)
pikachu.tackle(squirtle)